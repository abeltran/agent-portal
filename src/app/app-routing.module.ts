import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout/layout.component';
import { AuthGuard } from '@core/guards/auth.guard';
import { MainLoginComponent } from './pages/login/components/main-login/main-login.component';

const routes: Routes = [
  {
    path: 'login',
    component: MainLoginComponent
  },
  {
    path: 'agents',
    component: LayoutComponent,
    canActivate: [AuthGuard],
    children: [
      { path: '',
        redirectTo: 'panel/welcome',
        pathMatch: 'full',
      },
      {
        path: 'panel',
        loadChildren: () => import('./pages/agents/agents.module').then(m => m.AgentsModule)
      },
      {
        path: 'error',
        loadChildren: () => import('./pages/error-page/error-page.module').then(m => m.ErrorPageModule)
      },
    ]
  },
  {
    path: 'admin',
    component: LayoutComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        redirectTo: 'panel/welcome',
        pathMatch: 'full',
      },
      {
        path: 'panel',
        loadChildren: () => import('./pages/admin/admin.module').then(m => m.AdminModule)
      }
    ]
  },
  {
    path: '**',
    loadChildren: () => import('./pages/error-page/error-page.module').then(m => m.ErrorPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

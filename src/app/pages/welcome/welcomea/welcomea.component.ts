import { Component, OnInit } from '@angular/core';
import { fadeInAnimation } from '@animations/index';
@Component({
  selector: 'app-welcomea',
  animations: [fadeInAnimation],
  templateUrl: './welcomea.component.html',
  styleUrls: ['./welcomea.component.scss']
})
export class WelcomeaComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}

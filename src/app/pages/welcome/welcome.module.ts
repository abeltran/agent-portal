import { NgModule } from '@angular/core';

import { WelcomeRoutingModule } from './welcome-routing.module';

// import { WelcomeComponent } from './welcome.component';
import { SharedModule } from '@shared/shared.module';
// import { WelcomeaComponent } from './welcomea/welcomea.component';


@NgModule({
  imports: [WelcomeRoutingModule, SharedModule],
  declarations: [
    // WelcomeComponent,
    // WelcomeaComponent
  ],
  exports: [
    // WelcomeComponent
  ]
})
export class WelcomeModule { }

import { NgModule } from '@angular/core';

import { CommissionRoutingModule } from './commission-routing.module';
import { AgentCommissionComponent } from './components/agent-commission/agent-commission.component';
import { ModalAgentCommissionComponent } from './components/modal-agent-commission/modal-agent-commission.component';
import { SharedModule } from '@shared/shared.module';
import { CommonModule } from '@angular/common';


@NgModule({
  declarations: [AgentCommissionComponent, ModalAgentCommissionComponent],
  imports: [
    CommonModule,
    CommissionRoutingModule,
    SharedModule
  ]
})
export class CommissionModule { }

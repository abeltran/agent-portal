import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AgentCommissionComponent } from './components/agent-commission/agent-commission.component';


const routes: Routes = [{
  path: '',
  component: AgentCommissionComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CommissionRoutingModule { }

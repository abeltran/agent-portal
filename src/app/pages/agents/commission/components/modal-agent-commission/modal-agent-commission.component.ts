import { Component, OnInit, Input, OnChanges, Output, EventEmitter, ViewChild } from '@angular/core';
import * as moment from 'moment';
import { AgentService } from '@core/services/agent.service';
import { ExcelService } from '@core/services/excel.service';
import { meses } from '@shared/utils/dataF';
import { CommissionReportGet } from '../../../../../core/models/agent-data.model';

@Component({
  selector: 'app-modal-agent-commission',
  templateUrl: './modal-agent-commission.component.html',
  styleUrls: ['./modal-agent-commission.component.scss'],
})
export class ModalAgentCommissionComponent implements OnInit {
  isLoading = false;
  months: [] = meses;
  datos: CommissionReportGet[];
  selMonth: number;
  showNotification = false;
  notification: {
    type: string;
    title: string;
    message: string;
  };
  @Output() showModal: EventEmitter<any> = new EventEmitter();
  @Input() isModalVisible: boolean;
  constructor(private agentService: AgentService, private excelService: ExcelService) { }

  ngOnInit() {
  };

  handleCancel() {
    this.isModalVisible = !this.isModalVisible;
    // this.isLoading = false;
  }

  selMonths(event: any, index: number) {
    event.preventDefault();
    this.isLoading = true;
    setTimeout(() => {
      this.selMonth = index;
      this.agentService.obtainMontlyCommissions(index).subscribe(resp => {
      if ( resp['payload'].length > 0 ) {
        this.datos = resp['payload'];
        this.generateExcel();
        this.isLoading = false;
      } else {
        this.showOperationResults('error', 'Oops!', 'No hay información de este mes');
      }
    });
    }, 1200);
  }

  generateExcel() {
    this.excelService.generateExcel(this.selMonth, this.datos);
    this.handleCancel();
  }

  showOperationResults(
    type: string,
    title: string,
    message: string,
    ) {
    this.notification = {type, title, message};
    this.showNotification = true;
    this.isLoading = false;
  }

  hideNotification() {
    this.showNotification = false;
  }

}

import { Component, OnInit } from '@angular/core';
import { AgentService } from '@core/services/agent.service';
import { CommissionsGet } from '@core/models/agent-data.model';
import { Router } from '@angular/router';
import { fadeInAnimation } from '@animations/index';


@Component({
  selector: 'app-agent-commission',
  templateUrl: './agent-commission.component.html',
  animations: [fadeInAnimation],
  styleUrls: ['./agent-commission.component.scss'],
})
export class AgentCommissionComponent implements OnInit {

  fetchingData = true;
  isModalVisible = false;
  commissionData: CommissionsGet[];
  pageSize =  3;
  currentPage = 1;
  totalElements: number;
  retryRoute =  '/agents/panel/comision';

  constructor(private agentService: AgentService, private router: Router) { }

  ngOnInit() {
    this.getCommissions();
  }

  getCommissions() {
    return this.agentService.obtainCommissions().subscribe( commissions => {
      this.commissionData = commissions['payload'];
      this.fetchingData = false;
      this.totalElements = this.commissionData.length;
    }, () => {
      this.router.navigate(['/agents/error', this.retryRoute]);
    });
  }

  showModal() {
    this.isModalVisible = !this.isModalVisible;
  }

  // onChangeIndexPage(page: number) {
  //   this.getCommissions();
  // }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MaterialRoutingModule } from './material-routing.module';
import { AgentMaterialComponent } from './components/agent-material/agent-material.component';
import { SharedModule } from '../../../shared/shared.module';


@NgModule({
  declarations: [AgentMaterialComponent],
  imports: [
    CommonModule,
    SharedModule,
    MaterialRoutingModule
  ]
})
export class MaterialModule { }

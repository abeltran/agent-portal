import { Component, OnInit } from '@angular/core';
import { AgentService } from '../../../../../core/services/agent.service';
import { AgentDataModel } from '../../../../../core/models/agent-data.model';

@Component({
  selector: 'app-agent-material',
  templateUrl: './agent-material.component.html',
  styleUrls: ['./agent-material.component.scss'],
})
export class AgentMaterialComponent implements OnInit {
  loading = true;
  routePdf = 'assets/images/docs.png';
  routeVideo = 'assets/images/videp.png';
  routeImage = 'assets/images/photoi.png';
  manuales: AgentDataModel[];
  flyers: AgentDataModel[];
  videos: AgentDataModel[];
  matDat = {};

  constructor(private agentService: AgentService) {}

  ngOnInit() {
    this.fetchMaterialAgent();
  }

  fetchMaterialAgent() {
    this.matDat = this.agentService.obtainMaterial().subscribe((resp) => {
      this.manuales = resp['manuales'];
      this.flyers = resp['flyers'];
      this.videos = resp['videos'];
      this.loading = false;
      this.addExtensions();
    });
  }

  addExtensions() {
    this.manuales.map((r) => {
      r.ext = r.name.split('.').pop();
      r.namewo = r.name.split('.').shift();
      r.ext = r.ext.toLowerCase();
    });
    this.flyers.map((r) => {
      r.ext = r.name.split('.').pop();
      r.namewo = r.name.split('.').shift();
      r.ext = r.ext.toLowerCase();
    });
    this.videos.map((r) => {
      r.ext = r.name.split('.').pop();
      r.namewo = r.name.split('.').shift();
      r.ext = r.ext.toLowerCase();
    });
  }
}

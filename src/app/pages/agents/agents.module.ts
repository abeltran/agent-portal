import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AgentsRoutingModule } from './agents-routing.module';
import { WelcomeComponent } from '../welcome/welcome.component';
import { SharedModule } from '../../shared/shared.module';




@NgModule({
  declarations: [WelcomeComponent],
  imports: [
    CommonModule,
    AgentsRoutingModule,
    SharedModule,
  ],
})
export class AgentsModule { }

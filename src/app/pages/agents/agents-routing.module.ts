import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WelcomeComponent } from '../welcome/welcome.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'welcome',
        component: WelcomeComponent
      },
      {
        path: 'comision',
        loadChildren: () => import('./commission/commission.module').then(m => m.CommissionModule)
      },
      {
        path: 'afiliacion',
        loadChildren: () => import('./affiliation/affiliation.module').then(m => m.AffiliationModule)
      },
      {
        path: 'material',
        loadChildren: () => import('./material/material.module').then(m => m.MaterialModule)
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AgentsRoutingModule { }

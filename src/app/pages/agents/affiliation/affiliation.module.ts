import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AffiliationRoutingModule } from './affiliation-routing.module';
import { AgentAffiliationComponent } from './components/agent-affiliation/agent-affiliation.component';
import { SharedModule } from '../../../shared/shared.module';



@NgModule({
  declarations: [AgentAffiliationComponent],
  imports: [
    CommonModule,
    SharedModule,
    AffiliationRoutingModule
  ]
})
export class AffiliationModule { }

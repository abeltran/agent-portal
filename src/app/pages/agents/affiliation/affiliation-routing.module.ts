import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AgentAffiliationComponent } from './components/agent-affiliation/agent-affiliation.component';


const routes: Routes = [
  {
    path: '',
    component: AgentAffiliationComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AffiliationRoutingModule { }

import { Component, OnInit } from '@angular/core';
import { AffiliationsGet } from '@core/models/agent-data.model';
import { AgentService } from '@core/services/agent.service';

@Component({
  selector: 'app-agent-affiliation',
  templateUrl: './agent-affiliation.component.html',
  styleUrls: ['./agent-affiliation.component.scss']
})
export class AgentAffiliationComponent implements OnInit {

  fetchingData = true;
  isModalVisible = false;
  affiliationData: AffiliationsGet[];

  constructor(private agentService: AgentService) { }

  ngOnInit() {
    this.getAffiliations();
  }

  getAffiliations() {
    this.agentService.obtainAffiliations().subscribe(resp => {
      console.log(resp);
      this.affiliationData = resp['payload'];
      this.fetchingData = false;
    });
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoginRoutingModule } from './login-routing.module';
// import { MainLoginComponent } from './components/main-login/main-login.component';
// import { FormLoginComponent } from './components/form-login/form-login.component';

import { SharedModule } from '@shared/shared.module';

@NgModule({
  declarations: [
    // MainLoginComponent,
    // FormLoginComponent
  ],
  imports: [
    CommonModule,
    LoginRoutingModule,
    SharedModule
  ]
})
export class LoginModule { }

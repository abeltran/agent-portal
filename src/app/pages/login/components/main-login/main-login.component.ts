import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../../core/services/auth.service';
import { Router } from '@angular/router';
import { fadeInAnimation } from '@animations/index';

@Component({
  selector: 'app-main-login',
  templateUrl: './main-login.component.html',
  animations: [fadeInAnimation],
  styleUrls: ['./main-login.component.scss']
})
export class MainLoginComponent implements OnInit {

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {
    this.readSession();
  }

  readSession() {
    if (this.authService.estaAutenticado()) {
      this.router.navigate(['agents']);
    }
  }

  test() {
    // this.authService.sendEmailWelcome({user: 'Aldo'});
  }

}

import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../../../core/services/auth.service';
import { UsuarioModel } from '../../../../core/models/user.model';

import { NzMessageService } from 'ng-zorro-antd/message';

@Component({
  selector: 'app-form-login',
  templateUrl: './form-login.component.html',
  styleUrls: ['./form-login.component.scss'],
})
export class FormLoginComponent implements OnInit {
  validateForm: FormGroup;
  isLoading = false;
  usuario: UsuarioModel;

  showNotification = false;
  notification: {
    type: string;
    title: string;
    message: string;
  };

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private authService: AuthService,
    private message: NzMessageService
  ) {}

  ngOnInit() {
    this.usuario = new UsuarioModel();
    this.validateForm = this.fb.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]],
    });
  }

  submitForm(event: Event): void {
    event.preventDefault();

    const id = this.message.loading('Espere por favor', { nzDuration: 0 })
      .messageId;
    setTimeout(() => {
      // tslint:disable-next-line: forin
      for (const i in this.validateForm.controls) {
        this.validateForm.controls[i].markAsDirty();
        this.validateForm.controls[i].updateValueAndValidity();
      }

      // console.log(this.validateForm.value);
      this.authService.login(this.validateForm.value).subscribe((resp) => {
        if (resp.success) {
          this.router.navigate(['agents']);
        } else {
          this.showOperationResults('error', 'Oops!', resp.message);
        }
      });
      this.message.remove(id);
    }, 3500);
  }

  showOperationResults(
    type: string,
    title: string,
    message: string,
    ) {
    this.notification = {type, title, message};
    this.showNotification = true;
    this.isLoading = false;
  }

  hideNotification() {
    this.showNotification = false;
  }
}

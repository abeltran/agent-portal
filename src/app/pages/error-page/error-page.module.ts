import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ErrorPageRoutingModule } from './error-page-routing.module';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { NotAllowedComponent } from './components/not-allowed/not-allowed.component';
import { ServerErrorComponent } from './components/server-error/server-error.component';
import { SharedModule } from '@shared/shared.module';


@NgModule({
  declarations: [NotFoundComponent, NotAllowedComponent, ServerErrorComponent],
  imports: [
    CommonModule,
    ErrorPageRoutingModule,
    SharedModule
  ]
})
export class ErrorPageModule { }

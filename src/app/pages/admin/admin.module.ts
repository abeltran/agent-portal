import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { RegisterComponent } from './register/register.component';
import { SharedModule } from '../../shared/shared.module';
import { WelcomeaComponent } from '../welcome/welcomea/welcomea.component';


@NgModule({
  declarations: [RegisterComponent, WelcomeaComponent],
  imports: [
    CommonModule,
    AdminRoutingModule,
    SharedModule
  ]
})
export class AdminModule { }

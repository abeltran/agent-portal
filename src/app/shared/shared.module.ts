import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AntDesignModule } from '../ant-design/ant-design/ant-design.module';
import { NgxPaginationModule } from 'ngx-pagination';

import { NotificationComponent } from './components/notification/notification.component';



@NgModule({
  declarations: [
    NotificationComponent
  ],
  imports: [
    CommonModule,
    AntDesignModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule
  ],
  exports: [
    FormsModule,
    ReactiveFormsModule,
    AntDesignModule,
    NgxPaginationModule,
    NotificationComponent
  ]
})
export class SharedModule { }

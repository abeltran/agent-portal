import { Component, OnInit, Input, Output, ViewChild, EventEmitter, TemplateRef } from '@angular/core';
import { NzNotificationService } from 'ng-zorro-antd/notification';


@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnInit {

  @Input() type: string;
  @Input() title: string;
  @Input() errorMsg: string;
  @Output() hideNotification: EventEmitter<any> = new EventEmitter();
  @ViewChild('notifyMe', { static: true }) TemplateRef;
  timeOut = 700;

  constructor(private notification: NzNotificationService) { }

  ngOnInit() {
    this.showNotification(this.TemplateRef);
  }

  showNotification(template: TemplateRef<{}>) {
    this.notification.template(template, { nzDuration: this.timeOut });
    setTimeout(() => this.hideNotification.emit(), this.timeOut);
  }

}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../core/services/auth.service';
import { NzMessageService } from 'ng-zorro-antd/message';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss'],
})
export class LayoutComponent implements OnInit {
  isCollapsed = false;
  position = 'topLeft';

  constructor(
    private router: Router,
    private authService: AuthService,
    private message: NzMessageService
  ) {}

  ngOnInit() {}

  collapseMenu(isCollapsed: boolean) {
    this.isCollapsed = isCollapsed;
  }

  signOut() {
    const id = this.message.loading('Saliendo', { nzDuration: 0 }).messageId;
    setTimeout(() => {
      this.router.navigate(['login']);
      this.authService.logout();
      this.message.remove(id);
    }, 2500);
  }
}

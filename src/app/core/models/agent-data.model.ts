export class AgentDataModel {
    name: string;
    namewo: string;
    link: string;
    ext: string;
}

export class CommissionsGet {
  Fecha: string;
  Monto: number;
  ServicioID: number;
  Transacciones: number;
  BLMID: string;
}

export class CommissionReportGet {
  aidComercio: string;
  bNombreComercio: string;
  cStatus: string;
  dBonoRegistro: string;
  eBonoAfiliacion: string;
}
export class AffiliationsGet {
  FechaAfiliacion: string;
  FechaPrimerVAS: string;
  FechaLimiteFinanciero: string;
  AgentID: string;
  Dispositivo: string;
  FechaPrimerSF: string;
  FechaFinanciero: string;
  BLMID: number;
  NombreComercio: string;
  FechaLimiteVAS: string;
  Financiero: string;
  Agente: string;
}
export class UsuarioModel {
    username: string;
    password: string;
}

export class LoginPost {
    password: string;
    agentid: string;
    message: string;
    success: boolean;
}

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { AgentDataModel, CommissionsGet, CommissionReportGet } from '../models/agent-data.model';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class AgentService {

  options = {};

  constructor(private http: HttpClient) {

    const now = moment();
    const headers = new HttpHeaders({
      'Content-type': 'application/json',
      week: `${now.week()}`
    });

    this.options = { headers };
  }

   obtainMaterial() {
     return this.http.get<AgentDataModel>(environment.api + 'material', this.options);
   }

   obtainCommissions() {
     return this.http.get<CommissionsGet>(environment.api + 'commissions', this.options);
   }

   obtainMontlyCommissions(month: number) {
    return this.http.get<CommissionReportGet>(environment.api + `commission-status/${month}`, this.options);
  }

   obtainAffiliations() {
    return this.http.get(environment.api + 'affiliations', this.options);
  }

}

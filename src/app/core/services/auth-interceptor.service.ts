import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthInterceptorService {

  constructor(private router: Router) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const agentId: string = localStorage.getItem('agent-id');

    let request = req;

    if (agentId) {
       request = req.clone({
         setHeaders: {
          Authorization: 'Basic ' + btoa('abeltran@qiubo.mx:secret'),
          'agent-id': agentId
         }
      });
    }

    return next.handle(request).pipe(
      catchError((err: HttpErrorResponse) => {

        if (err.status === 401) {
          localStorage.removeItem('token');
          localStorage.removeItem('agent-id');
          this.router.navigateByUrl('/login');
        }

        return throwError( err );

      })
    );
  }
}

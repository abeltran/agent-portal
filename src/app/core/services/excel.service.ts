import { Injectable } from '@angular/core';
import { Workbook } from 'exceljs';
import { DatePipe } from '@angular/common';
import * as fs from 'file-saver';
import * as logoFile from '../../shared/utils/qiubologo';
import { meses } from '@shared/utils/dataF';
import { CommissionReportGet } from '../models/agent-data.model';

@Injectable({
  providedIn: 'root',
})
export class ExcelService {
  nameM: string;
  constructor(private datePipe: DatePipe) {}

  generateExcel(mes: number, datos: CommissionReportGet[]) {
    
    const data = [];
    datos.forEach(d => {
      const values = [d.aidComercio, d.bNombreComercio, d.cStatus, d.dBonoRegistro, d.eBonoAfiliacion];
      data.push(values);
    });
    
    // console.table('from excel service: ' + JSON.stringify(datos));
    this.nameM = meses[mes -= 1].name;
    // Excel Title, Header, Data
    const title = `Comisiones Mes ${this.nameM}`;
    const header = ['ID COMERCIO', 'NOMBRE COMERCIO', 'STATUS', 'BONO REGISTRO', 'BONO AFILIACIÓN'];
    // Create workbook and worksheet
    const workbook = new Workbook();
    const worksheet = workbook.addWorksheet(this.nameM);
    // Add Row and formatting
    const titleRow = worksheet.addRow([title]);
    titleRow.font = {
      name: 'Calibri',
      family: 4,
      size: 16,
      underline: 'double',
      bold: true,
    };
    worksheet.addRow([]);
    const subTitleRow = worksheet.addRow([
      'Fecha : ' + this.datePipe.transform(new Date(), 'medium'),
    ]);
    // Add Image
    const logo = workbook.addImage({
      base64: logoFile.logoBase64,
      extension: 'png',
    });
    worksheet.addImage(logo, 'E1:F3');
    worksheet.mergeCells('A1:D2');
    // Blank Row
    worksheet.addRow([]);
    // Add Header Row
    const headerRow = worksheet.addRow(header);

    // Cell Style : Fill and Border
    headerRow.eachCell((cell, number) => {
      cell.fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: 'FFFFFF00' },
        bgColor: { argb: '313e48' },
      };
      cell.border = {
        top: { style: 'thin' },
        left: { style: 'thin' },
        bottom: { style: 'thin' },
        right: { style: 'thin' },
      };
    });
    // worksheet.addRows(data);
    // Add Data and Conditional Formatting

    data.forEach((d) => {

      const row = worksheet.addRow(d);
      const qty = row.getCell(5);
      let color = 'FF9999';
      if (+qty.value >= 5000) {
        color = 'FF99FF99';
      } else if (+qty.value >= 2001 && +qty.value <= 4999 ) {
        color = 'FFF933';
      }
      qty.fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: color },
      };
    });
    worksheet.getColumn(3).width = 30;
    worksheet.addRow([]);
    // Footer Row
    const footerRow = worksheet.addRow([
      'Hoja de Excel generada por sistema',
    ]);
    footerRow.getCell(1).fill = {
      type: 'pattern',
      pattern: 'solid',
      fgColor: { argb: 'FFCCFFE5' },
    };
    footerRow.getCell(1).border = {
      top: { style: 'thin' },
      left: { style: 'thin' },
      bottom: { style: 'thin' },
      right: { style: 'thin' },
    };
    // Merge Cells
    worksheet.mergeCells(`A${footerRow.number}:F${footerRow.number}`);
    // Generate Excel File with given name
    workbook.xlsx.writeBuffer().then((data) => {
      const blob = new Blob([data], {
        type:
          'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
      });
      fs.saveAs(blob, `Estado de cuenta ${this.nameM}.xlsx`);
    });
  }
}

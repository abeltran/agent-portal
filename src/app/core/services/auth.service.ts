import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { UsuarioModel, LoginPost } from '../models/user.model';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import '../../../assets/smtp.js';
import * as bcrypt from 'bcryptjs';
import { Observable } from 'rxjs';

declare let Email: any;

@Injectable({
  providedIn: 'root',
})
export class AuthService {

  agentInfo: LoginPost;

  constructor(private http: HttpClient) {
    this.agentInfo = new LoginPost();
    this.readToken();
  }


  sendEmailWelcome(userInfo) {

    Email.send({
    Host : 'smtp.office365.com',
    Username : 'portaltest@bluelabel.mx',
    Password : 'PORTAL.2020',
    To : 'beltranaldo404@gmail.com',
    From : `Agents Portal portaltest@bluelabel.mx`,
    Subject : 'Bienvenido a Agents Portal',
    Body : `
    <i>Hola .</i> <br/> <b>Nombre: </b> ALDO <br />
    <b>Email: </b> beltranaldo404@gmail.com <br />
    <b>Password: </b>TEST <br/>`
    }).then( message => {
      console.log(message);
    } );
  }

  readToken() {
    if (localStorage.getItem('agent-id')) {
      // Verify if exists agent-id on localstorage
      this.agentInfo.agentid = localStorage.getItem('agent-id');
      // this.agentId = localStorage.getItem('agent-id');
    } else {
      this.agentInfo.agentid = '';
    }

    return this.agentInfo;
  }

  logout() {
    localStorage.removeItem('expira');
    localStorage.removeItem('agent-id');
  }

  login(usuario: UsuarioModel) {

    const headers = new HttpHeaders({
      'Content-type': 'application/json',
      Authorization: 'Basic ' + btoa('abeltran@qiubo.mx:secret'),
    });

    const options = { headers };

    const authData = {
      username: usuario.username,
    };

    const cc = new LoginPost();

    return this.http.post<LoginPost>(environment.api + 'login', authData, options).pipe(
      map((resp) => {
        if (resp.password !== null) {
          if (this.verifyHash(usuario.password, resp.password[0], resp.agentid[0])) {
            this.storeAgent(resp.agentid);
            resp.success = true;
            return resp;
          } else {
            cc.success = false;
            cc.message = 'Contraseña incorrecta';
            return cc;
         }
        } else {
          cc.success = false;
          cc.message = 'Email incorrecto';
          return cc;
        }
      })
    );
  }

  private verifyHash(pwd: string, hash: string, agentId: string): any {

    const val = bcrypt.compareSync(pwd, hash);
    return val;
  }

  private storeAgent(agentId: string) {
    this.agentInfo.agentid = agentId;
    localStorage.setItem('agent-id', agentId);

    const hoy = new Date();
    // Set an hour to assign token's time expiration
    hoy.setSeconds(3600);

    // Almacenar token en formato string
    localStorage.setItem('expira', hoy.getTime().toString());
  }

  estaAutenticado(): boolean {

    if (!this.agentInfo.agentid && this.agentInfo.agentid.length < 2) { return false; }

    const expira = Number(localStorage.getItem('expira'));
    const expirateDate = new Date();

    expirateDate.setTime(expira);
    // Mientras sea mayor la hora almacenada a la actual esta vigente
    if (expirateDate > new Date()) {
      return true;
    } else {
      return false;
    }
  }
}

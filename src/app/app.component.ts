import { Component } from '@angular/core';
import { fadeInAnimation } from '@animations/index';

@Component({
  selector: 'app-root',
  animations: [fadeInAnimation],
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  isCollapsed = false;
}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { LayoutComponent } from './layout/layout.component';
import { environment } from './../environments/environment';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { IconsProviderModule } from './icons-provider.module';
import { NZ_I18N, es_ES } from 'ng-zorro-antd';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { registerLocaleData, DatePipe } from '@angular/common';
import es from '@angular/common/locales/es';
import { SharedModule } from '@shared/shared.module';
import { MainLoginComponent } from './pages/login/components/main-login/main-login.component';
import { FormLoginComponent } from './pages/login/components/form-login/form-login.component';
import { AuthInterceptorService } from './core/services/auth-interceptor.service';


registerLocaleData(es);


@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
    MainLoginComponent,
    FormLoginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    IconsProviderModule,
    HttpClientModule,
    SharedModule
  ],
  providers: [
    DatePipe,
    { provide: 'API_URL', useValue: environment.api },
    { provide: NZ_I18N, useValue: es_ES },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptorService,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
